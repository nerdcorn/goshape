package goshape

import (
	"errors"
	"fmt"
	"reflect"
)

func Initialize(conf interface{}) error {
	var v reflect.Value
	var numField int
	t := reflect.TypeOf(conf)
	switch t.Kind() {
	case reflect.Ptr:
		v = reflect.ValueOf(conf)
		numField = t.Elem().NumField()
	case reflect.Struct:
		v = conf.(reflect.Value)
		numField = v.Elem().NumField()
	}

	for i := 0; i < numField; i++ {
		var f reflect.StructField
		switch t.Kind() {
		case reflect.Ptr:
			f = t.Elem().Field(i)
		case reflect.Struct:
			f = v.Type().Elem().Field(i)
		}
		typeOf := v.Elem().Field(i).Kind()
		def, ok := f.Tag.Lookup("default")
		if !ok && typeOf != reflect.Struct && typeOf != reflect.Interface {
			return errors.New(fmt.Sprintf("Unable to find default tag in struct for field: %s", f.Name))
		}

		env, ok := f.Tag.Lookup("env")
		if !ok && typeOf != reflect.Struct && typeOf != reflect.Interface {
			return errors.New(fmt.Sprintf("Unable to find environment variable in struct for field: %s", f.Name))
		}

		if v.Elem().Field(i).CanSet() {
			switch typeOf {
			case reflect.Bool:
				v.Elem().Field(i).SetBool(GetEnvBool(env, def))
			case reflect.Struct:
				conf2 := reflect.New(v.Elem().Field(i).Type())
				if err := Initialize(conf2); err != nil {
					return err
				}
				v.Elem().Field(i).Set(reflect.Indirect(conf2))
			case reflect.Int64:
				switch f.Type.String() {
				case "time.Duration":
					v.Elem().Field(i).Set(reflect.ValueOf(GetEnvTimeout(env, def)))
				default:
					v.Elem().Field(i).SetInt(GetEnvInt64(env, def))
				}
			case reflect.Float64:
				v.Elem().Field(i).SetFloat(GetEnvFloat64(env, def))
			case reflect.String:
				v.Elem().Field(i).SetString(GetEnv(env, def))
			}
		}
	}

	return nil
}

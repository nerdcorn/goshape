package goshape

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"time"
)

type (
	Database struct {
		User         string `env:"DB_USER" default:""`
		Password     string `env:"DB_PASSWORD" default:""`
		Host         string `env:"DB_HOST" default:""`
		DatabaseName string `env:"DB_NAME" default:""`
		Port         int64  `env:"DB_PORT" default:"5432"`
		Driver       string `env:"DB_DRIVER" default:"postgres"`
	}

	Conf struct {
		/* Database Configs */
		Database Database
		/* Service Level Configs */
		ServiceName            string        `env:"SERVICE_NAME" default:""`
		FloatingPointPrecision float64       `env:"FLOATING_POINT" default:"19.245"`
		Timeout                time.Duration `env:"TIMEOUT" default:"30s"`
		IsTest                 bool          `env:"IS_TEST" default:"false"`
	}
)

func TestInitialize(t *testing.T) {
	_ = os.Setenv("DB_USER", "notroot")
	_ = os.Setenv("SERVICE_NAME", "My Test App")
	_ = os.Setenv("IS_TEST", "true")

	conf := Conf{}
	if err := Initialize(&conf); err != nil {
		panic(err)
	}

	timeout, _ := time.ParseDuration("30s")
	conf2 := Conf{
		Database: Database{
			User:         "notroot",
			Password:     "",
			Host:         "",
			DatabaseName: "",
			Port:         5432,
			Driver:       "postgres",
		},
		ServiceName:            "My Test App",
		FloatingPointPrecision: 19.245,
		Timeout:                timeout,
		IsTest:                 true,
	}

	assert.Equal(t, conf2.Database.User, conf.Database.User)
	assert.Equal(t, conf2.Database.Password, conf.Database.Password)
	assert.Equal(t, conf2.Database.Host, conf.Database.Host)
	assert.Equal(t, conf2.Database.DatabaseName, conf.Database.DatabaseName)
	assert.Equal(t, conf2.Database.Port, conf.Database.Port)
	assert.Equal(t, conf2.Database.Driver, conf.Database.Driver)
	assert.Equal(t, conf2.ServiceName, conf.ServiceName)
	assert.Equal(t, conf2.FloatingPointPrecision, conf.FloatingPointPrecision)
	assert.Equal(t, conf2.Timeout, conf.Timeout)
	assert.Equal(t, conf2.IsTest, conf.IsTest)
}

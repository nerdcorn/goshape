# goshape

## Overview

The `goshape` Golang package is a simple config struct parser. 

It allows you to annotate a struct with simple environment labels and default values. `goshape` then utilizes 
reflection to introspect the struct, look up the environment variables, and apply defaults where applicable. 


```go
// In your config/config.go file
import "gitlab.com/nerdcorn/goshape"

type MyConfig struct {
	AppName     string `env:"APPNAME" default:"My Test App"`
	ServiceHost int64  `env:"SERVICE_HOST" default:"8080"`
}

var Config MyConfig

// Make sure you use the init so your config is initialized on startup
func init() {
    if err := goshape.Initialize(Config); err != nil {
    	// Handle your error here.
    }	
}
```

You can also have your config struct nested in order to better separate domains within your config structure.

```go
type (
	Database struct {
		Username string `env:"DB_USERNAME" default:"root"`
		Password string `env:"DB_PASSWORD" default:"goshape"`
		Host     string `env:"DB_HOST" default:"localhost"`
		Port     int64  `env:"DB_PORT" default:"5432"`
	}
	
	MyConfig struct {
    	AppName     string `env:"APPNAME" default:"My Test App"`
    	ServiceHost int64  `env:"SERVICE_HOST" default:"8080"`
    	Database    Database
    }
)
```

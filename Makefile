GOPATH ?= ~/go

test:
	mkdir -p coverage/
	go test ./... -v -coverprofile coverage/coverage.cov

coverage: test
	go tool cover -func=coverage/coverage.cov

coverageHtml: coverage
	go tool cover -html=coverage/coverage.cov -o coverage/coverage.html

lint: ## Lint the files
	${GOPATH}/bin/golint ./... | grep -v "should have comment" || true

fmt: ## Format
	go fmt ./...

deps: ## Get the dependencies
	@go get -v -d ./...
	@go get -u golang.org/x/lint/golint

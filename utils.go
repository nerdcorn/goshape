package goshape

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func GetEnv(env, def string) string {
	var v string
	if v = os.Getenv(env); v == "" {
		return def
	}
	return v
}

func GetEnvInt64(env, def string) int64 {
	var v int64
	var x string
	var err error
	if x = os.Getenv(env); x == "" {
		x = def
	}
	if v, err = strconv.ParseInt(x, 10, 64); err != nil {
		panic(fmt.Sprintf("An error converting your environment from string to int occurred: %s", err))
	}
	return v
}

func GetEnvFloat64(env, def string) float64 {
	var v float64
	var x string
	var err error
	if x = os.Getenv(env); x == "" {
		x = def
	}
	if v, err = strconv.ParseFloat(x, 64); err != nil {
		panic(fmt.Sprintf("An error converting your environment from string to int occurred: %s", err))
	}
	return v
}

func GetEnvTimeout(env, def string) time.Duration {
	var v time.Duration
	var x string
	var err error
	if x = os.Getenv(env); x == "" {
		x = def
	}
	if v, err = time.ParseDuration(x); err != nil {
		panic(fmt.Sprintf("An error converting your environment from string to duration occurred: %s", err))
	}

	return v
}

func GetEnvBool(env, def string) bool {
	var v bool
	var x string
	var err error
	if x = os.Getenv(env); x == "" {
		x = def
	}
	if v, err = strconv.ParseBool(x); err != nil {
		panic(fmt.Sprintf("An error converting your environment variable from string to boolean occurred: " +
			"%s", err))
	}
	return v
}
